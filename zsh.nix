# Shell configuration for zsh (frequently used)
{ config, lib, pkgs, ... }: {
  # zsh settings
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
  
    enableCompletion = true;
    history.extended = true;

    initExtraBeforeCompInit = "export NIX_PATH=$HOME/.nix-defexpr/channels";
    initExtra = ''
      export TERM="xterm-256color"
      bindkey -e

      # Nix setup (environment variables, etc.)
      if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then
        . ~/.nix-profile/etc/profile.d/nix.sh
      fi

      # Add Env
      if [ -e ~/.env ]; then
        set -o allexport
        source ~/.env
        set +o allexport
      fi

      # Start starship
      eval "$(starship init zsh)"

      # Completions
      source <(kubectl completion zsh)
      source <(helm completion zsh)

      # turbogit -- see ./git.nix
      source <(tug completion zsh)

      # AM CI/CD tooling
      fpath+=~/.zfunc

      # Admiral Money Shipping schemas
      update-shipping-schema(){
        mkdir -p "$HOME/.schemas"
        curl --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/41432235/repository/files/dist%2Fshipping%2Dfile%2Dschema%2Ejson/raw" > "$HOME/.schemas/shipping-file-schema.json"
        echo "Pulled latest version of schema to '~/.schemas/shipping-file-schema.json'"
        echo "(Now ensure your vscode user settings point to this file)"
      }
    '';
  };
}
