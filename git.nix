# Git settings
{ config, lib, pkgs, ... }: {
  programs.git = {
    enable = true;
    userName = "James King";
    userEmail = "james.king@admiralfinancialservices.co.uk";

    # Replaces ~/.gitignore
    ignores = [
      # Allowlisting gitignore template for GO projects prevents us
      # from adding various unwanted local files, such as generated
      # files, developer configurations or IDE-specific files etc.
      #
      # Recommended: Go.AllowList.gitignore

      # Ignore everything
      "*"

      # But not these files...
      "!/.gitignore"

      "!*.go"
      "!go.sum"
      "!go.mod"

      "!*.yml"
      "!*.yaml"

      "!README.md"
      "!LICENSE"
      "!CODEOWNERS"
      "!Dockerfile"
      "!Makefile"

      # ...even if they are in subdirectories
      "!*/"

      # Don't commit any local .env files
      "*.env"
    ];

    # Global Git config
    extraConfig = {
      pull.rebase = "true";
      push.autoSetupRemote = "true";
      url."git@gitlab.com:afsl".insteadOf = "https://gitlab.com/afsl";
      init.defaultBranch = "main";

      alias = {
        update = "fetch --all --prune --prune-tags";
        lg =
          "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all";
      };
    };
  };
  programs.git-cliff = { enable = true; };

  home.packages = with pkgs; [ turbogit ];
}
