# Visual Studio Code settings
{ config, lib, pkgs, ... }: {
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    enableUpdateCheck = false;
    enableExtensionUpdateCheck = false;
    userSettings = {
      # aws
      "aws.suppressPrompts" = { "regionAddAutomatically" = true; };
      "aws.codeWhisperer.shareCodeWhispererContentWithAWS" = false;
      "aws.codeWhisperer.includeSuggestionsWithCodeReferences" = false;
      "aws.codeWhisperer.importRecommendation" = false;
      "aws.telemetry" = false;
      "boot-java.rewrite.reconcile" = true;

      "debug.terminal.clearBeforeReusing" = true;

      # Docker
      "docker.composeCommand" = "docker compose";
      "docker.dockerPath" = "${config.home.profileDirectory}/bin/docker";

      # Editor settings
      "editor.minimap.enabled" = false;
      "editor.fontFamily" =
        "'JetBrainsMono Nerd Font', Monaco, 'Courier New', monospace";
      "editor.codeLensFontFamily" =
        "'JetBrainsMono Nerd Font', Monaco, 'Courier New', monospace";
      "editor.fontLigatures" = true;
      "editor.fontSize" = 14;
      "editor.semanticHighlighting.enabled" = true;
      "editor.codeActionsOnSave" = {
        "source.fixAll" = true;
        "source.organizeImports" = true;
      };
      "files.associations" = { "*.hcl" = "terraform"; };
      "update.mode" = "none";

      # Git (and graph)
      "git.confirmSync" = false;
      "git-graph.dialog.fetchRemote.pruneTags" = true;
      "git-graph.dialog.fetchRemote.prune" = true;
      "git-graph.dialog.merge.squashCommits" = true;
      "git-graph.dialog.merge.noFastForward" = false;
      "git-graph.maxDepthOfRepoSearch" = 5;
      "git-graph.date.format" = "Relative";
      "git-graph.openToTheRepoOfTheActiveTextEditorDocument" = true;
      "git-graph.repository.fetchAndPrune" = true;
      "git-graph.repository.fetchAndPruneTags" = true;
      "git-graph.repositoryDropdownOrder" = "Name";

      # Gitlab
      "gitlab.showPipelineUpdateNotifications" = true;

      # Go settings
      "go.useLanguageServer" = true;
      "go.coverOnSingleTest" = true;
      "go.enableCodeLens" = { "runtest" = true; };
      "go.coverOnSingleTestFile" = true;
      "go.autocompleteUnimportedPackages" = true;
      "go.testExplorer.enable" = true;
      "go.testExplorer.alwaysRunBenchmarks" = true;
      "go.testExplorer.showDynamicSubtestsInEditor" = true;

      "go.addTags" = { "promptForTags" = true; };
      "go.survey.prompt" = false;
      "go.diagnostic.vulncheck" = "Imports";
      "go.toolsManagement.go" = "${config.home.profileDirectory}/bin/go";
      "go.toolsManagement.autoUpdate" = false;

      "go.alternateTools" = {
        "go" = "${config.home.profileDirectory}/bin/go";
        "dlv" = "${config.home.profileDirectory}/bin/dlv";
        "gopls" = "${config.home.profileDirectory}/bin/gopls";
        "staticcheck" = "${config.home.profileDirectory}/bin/staticcheck";
        "gotests" = "${config.home.profileDirectory}/bin/gotests";
        "impl" = "${config.home.profileDirectory}/bin/impl";
        "gomodifytags" = "${config.home.profileDirectory}/bin/gomodifytags";
        "go-outline" = "${config.home.profileDirectory}/bin/go-outline";
      };

      "gopls" = {
        "build.allowModfileModifications" = true;
        "build.env" = { "GOPRIVATE" = "gitlab.com/afsl/*"; };
        "ui.codelenses" = {
          "test" = true;
          "vendor" = false;
        };
        "ui.completion.usePlaceholders" = true;
        "ui.diagnostic.staticcheck" = true;
        "ui.diagnostic.analyses" = {
          "fieldalignment" = true;
          "nilness" = true;
          "shadow" = true;
          "unusedparams" = true;
          "unusedwrite" = true;
          "useany" = true;
        };
        "formatting.gofumpt" = true;
      };

      # Misc
      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "${config.home.profileDirectory}/bin/rnix-lsp";
      "redhat.telemetry.enabled" = false;

      # Security
      "security.workspace.trust.untrustedFiles" = "open";

      # Terminal
      "terminal.integrated.fontFamily" = "JetBrainsMono Nerd Font";
      "terminal.integrated.fontSize" = 13;
      "terminal.integrated.fontWeightBold" = "normal";
      "terminal.integrated.cursorStyle" = "underline";
      "terminal.integrated.allowChords" = false;
      "terminal.integrated.commandsToSkipShell" =
        [ "workbench.action.quickOpenView" ];

      # workbench
      "workbench.colorTheme" = "Moegi Dark";
      "workbench.startupEditor" = "none";
      "workbench.iconTheme" = "vscode-icons";

      #yaml
      "yaml.format.singleQuote" = true;
      "yaml.suggest.parentSkeletonSelectedFirst" = true;
      "yaml.customTags" = [
        "!And"
        "!And sequence"
        "!If"
        "!If sequence"
        "!Not"
        "!Not sequence"
        "!Equals"
        "!Equals sequence"
        "!Or"
        "!Or sequence"
        "!FindInMap"
        "!FindInMap sequence"
        "!Base64"
        "!Join"
        "!Join sequence"
        "!Cidr"
        "!Ref"
        "!Sub"
        "!Sub sequence"
        "!GetAtt"
        "!GetAZs"
        "!ImportValue"
        "!ImportValue sequence"
        "!Select"
        "!Select sequence"
        "!Split"
        "!Split sequence"
      ];
      "vsicons.dontShowNewVersionMessage" = true;
      "vs-kubernetes" = { "vs-kubernetes.crd-code-completion" = "disabled"; };
    };

    extensions = with pkgs.vscode-extensions; [
      golang.go
      gitlab.gitlab-workflow
      hashicorp.terraform
      iciclesoft.workspacesort
      jnoortheen.nix-ide
      mhutchie.git-graph
      redhat.vscode-yaml
      redhat.vscode-xml
      redhat.java
      vscjava.vscode-gradle
      waderyan.gitblame
      vscodevim.vim
    ];
  };
}