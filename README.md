# Development Environment

### Pre-requisites

You'll be required to install 2 things first in order to get things setup nicely.

 - [Nix Package Manager](https://nixos.org/download.html#nix-install-macos)
 - [Home Manager](https://nix-community.github.io/home-manager/index.html#sec-install-standalone)

Once these are install correctly you can clone this repo into `~/.config/home-manager` 

```shell
git clone git@gitlab.com:jamesking-afsl/nix.git ~/.config/home-manager
```
and run `home-manager switch`

## Useful tooling

Once everything is installed, you'll likely want to run:
```shell
pipx install --index-url "https://__token__:$GITLAB_ACCESS_TOKEN@gitlab.com/api/v4/projects/43686784/packages/pypi/simple" rpc
```
This installs our deploy cli tool buitl by the wonderful @tomafsl 

### Notes

MacOS has a frustrating bug that happens with every OS update. It wipes out your `/etc/zshrc` file which has lines relating to running the nix daemon.

If you update and notice that your nix commands no longer work, you'll need to take the follow steps (until Apple decide they're going to respect users settings...)

 - Open AFSL App and Request Admin (sudo) access
 - Once approved, run:
 ```shell
sudo nano /etc/zshrc
 ```
  - append the follow snippet to the end of your `/etc/zshrc` file
```shell
# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  source '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
# End Nix
```
 - Profit!


