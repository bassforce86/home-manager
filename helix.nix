{ config, pkgs, lib, ... }:

{
  programs.helix = {
    enable = true;
    defaultEditor = true;
    settings = {
      theme = "gruvbox_dark_hard";
      editor = {
        line-number= "relative";
        lsp.display-messages = true;
      };
    };
 };
}
