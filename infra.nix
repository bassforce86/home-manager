{ pkgs, ... }: {
  home.packages = with pkgs; [
    # Deployment Tools=
    awscli2
    aws-sso-cli
    kubectl
    kubectx
    kubernetes-helm
    kustomize
    terraform
    terraform-ls
    terragrunt
    # required by Colima
    colima
    docker
    qemu
  ];
}
