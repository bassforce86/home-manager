{ pkgs, ... }: {
  programs.ssh = {
    enable = true;
    matchBlocks = {
      "gitlab.com" = {
        hostname = "gitlab.com";
        user = "git";
        identityFile = "~/.ssh/afsl";
        extraOptions = { PreferredAuthentications = "publickey"; };
      };
      "bf86.gitlab.com" = {
        hostname = "gitlab.com";
        user = "git";
        identityFile = "~/.ssh/id_ed25519";
        extraOptions = { PreferredAuthentications = "publickey"; };
      };
      "bitbucket.org" = {
        hostname = "bitbucket.org";
        user = "JamesKing11";
        identityFile = "~/.ssh/afsl";
      };
      "i-* mi-*" = {
        proxyCommand = ''
          sh -c "aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters 'portNumber=%p'"'';
      };
    };
  };

  home.packages = with pkgs; [ ssm-session-manager-plugin ];
}
