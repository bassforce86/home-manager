{ config, pkgs, lib, ... } :

{
  programs.kitty = {
    enable = true;
    darwinLaunchOptions = [
      "--single-instance"
    ];
    theme = "Gruvbox Material Dark Hard";
    font = { 
      name = "JetBrainsMono Nerd Font"; 
      size = 13;
    };
    shellIntegration = {
      enableZshIntegration = true;
    };
  };
}