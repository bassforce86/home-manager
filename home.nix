{ config, lib, pkgs, ... }: {
  # allow non-free program installation
  nixpkgs.config = {
    allowUnfree = true;
    # allowBroken = true;
    # allowUnsupportedSystem = true;
  };

  imports = [
    ./git.nix
    ./go.nix
    ./infra.nix
    ./helix.nix
    ./java.nix
    ./kitty.nix
    ./python.nix
    ./ssh.nix
    ./starship.nix
    ./vscode.nix
    ./zsh.nix
  ];

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home = {
    username = "jk";
    homeDirectory = "/Users/jk";
    sessionPath = [
      "/Applications/Sublime Text.app/Contents/SharedSupport/bin"
      "\${config.home.homeDirectory}/scripts/hyperkit/build"
      "\${config.home.homeDirectory}/.local/bin"
      "\${config.home.profileDirectory}/bin"
      "\${config.home.homeDirectory}/.tfenv/bin"
    ];
    stateVersion = "23.05";

    packages = with pkgs; [
      (pkgs.nerdfonts.override { fonts = [ "FiraCode" "JetBrainsMono" ]; })
      nixfmt # Nix
      rnix-lsp
      # Shell
      helix
      kitty
      jq
      ripgrep
      fzf
      zsh
      zsh-autosuggestions
      zsh-syntax-highlighting
    ];

    # Set all shell aliases programatically
    shellAliases = {
      # Aliases for commonly used tools
      grep = "grep --color=auto";
      diff = "diff --color=auto";
      ll = "ls -lh";
      tf = "terraform";
      tg = "terragrunt";
      k = "kubectl";

      # check for updates
      update =
        "nix-channel --update --verbose && home-manager switch && source ~/.zshrc";
      # use a specific package on-top of current shell
      use = "nix-shell -p";
      # Reload home manager and zsh
      reload = "home-manager switch && source ~/.zshrc";
      # Nix garbage collection
      clean = "nix-collect-garbage -d";
      # proxy
      proxy =''
        export HTTPS_PROXY=http://127.0.0.1:8009 \
        export HTTP_PROXY=http://127.0.0.1:8009 \
        export http_proxy=http://127.0.0.1:8009 \ 
        export https_proxy=http://127.0.0.1:8009
      '';
      
      noproxy = ''
        export HTTPS_PROXY="" \ 
        export HTTP_PROXY="" \
        export http_proxy="" \ 
        export https_proxy="" 
      '';

      colup = ''
        export HTTPS_PROXY="" HTTP_PROXY="" http_proxy="" https_proxy="" && \
        colima start --cpu 6 --memory 12 --disk 60 --kubernetes --network-address
      '';

      coldn = "colima stop";

      start = "noproxy && update && codium ~/projects/src/gitlab.com/afsl/quote.code-workspace";
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.command-not-found.enable = true;
}
