{ config, pkgs, lib, ... }: {
  programs.go = {
    enable = true;
    package = pkgs.go_1_21;
    goPrivate = [ "gitlab.com/afsl/*" "bitbucket.org/firstcarquote/*" ];
  };
  home.packages = with pkgs; [
    # hot reload
    (air.override {buildGoModule = pkgs.buildGo121Module;})
    # formatter
    (gofumpt.override {buildGoModule = pkgs.buildGo121Module;})
    # Golang
    (delve.override {buildGoModule = pkgs.buildGo121Module;})
    (gopls.override {buildGoModule = pkgs.buildGo121Module;})
    (go-tools.override {buildGoModule = pkgs.buildGo121Module;})
    (go-swagger.overrideAttrs (_:
      let
        version = "master";
        src = pkgs.fetchFromGitHub {
          owner = "go-swagger";
          repo = "go-swagger";
          rev = "${version}";
          sha256 = "sha256-cVQB9B6JFnaeIumbeKZVFQMCBIzVWcldCQB94fdEfQc=";
        };
      in rec {
        name = "go-swagger";
        inherit src;
        inherit (pkgs.buildGo121Module{
          inherit src name;
          vendorSha256 = "sha256-TqoTzxPGF0BBUfLtYWkljRcmr08m4zo5iroWMklxL7U=";
        });
      }
    ))
  ];
}
