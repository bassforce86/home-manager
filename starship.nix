{ pkgs, ... }: {
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    # Configuration written to ~/.config/starship.toml
    settings = {
      command_timeout = 1200;
      kubernetes = {
        disabled = false;
        symbol = " ";
      };
      aws = { symbol = "  "; };
      docker_context = { symbol = " "; };
      nix_shell = { symbol = " "; };
      golang = { symbol = " "; };
      python = { symbol = " "; };
      git_branch = { symbol = " "; };
      shlvl = { symbol = " "; };
      package = { symbol = " "; };
      terraform = { symbol = " "; };
    };
  };
}
