{ pkgs, ... }: {
  home.packages = with pkgs; [
    # Python
    pipenv
    (python311.withPackages (ps:
      with ps; [
        python311Packages.flake8
        python311Packages.jedi
        python311Packages.pip
        python311Packages.pipx
        python311Packages.allure-behave
        python311Packages.behave
        python311Packages.requests
        python311Packages.python-lsp-black
        python311Packages.python-lsp-jsonrpc
        python311Packages.python-lsp-server
      ]))
  ];
}
